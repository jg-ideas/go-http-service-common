<a name="unreleased"></a>
## [Unreleased]


<a name="v0.0.3"></a>
## [v0.0.3] - 2021-08-29
### Added
- **all:** repository-related files

### Documentation
- release process

### Fixed
- **session:** lint error


<a name="v0.0.2"></a>
## [v0.0.2] - 2021-02-20

<a name="v0.0.1"></a>
## v0.0.1 - 2021-02-20

[Unreleased]: https://gitlab.com/jg-ideas/go-http-service-common/compare/v0.0.3...HEAD
[v0.0.3]: https://gitlab.com/jg-ideas/go-http-service-common/compare/v0.0.2...v0.0.3
[v0.0.2]: https://gitlab.com/jg-ideas/go-http-service-common/compare/v0.0.1...v0.0.2
