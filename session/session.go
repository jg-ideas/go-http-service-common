package session

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/gorilla/sessions"
)

// CookieStore will save session data as cookies
type CookieStore struct {
	store      *sessions.CookieStore
	cookieName string
}

// NewCookieStore creates a new *CookieStore
func NewCookieStore(cookieName string, keys ...[]byte) *CookieStore {
	return &CookieStore{
		store:      sessions.NewCookieStore(keys...),
		cookieName: cookieName,
	}
}

// Set lookup key and value in the CookieStore.
func (s *CookieStore) Set(lookupKey string, data interface{}, r *http.Request, w http.ResponseWriter) error {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("could not marshal session data [%s]:[%v]: %w", lookupKey, data, err)
	}

	session, err := s.store.Get(r, s.cookieName)
	if err != nil {
		return fmt.Errorf("could not decode session: %w", err)
	}

	session.Values[lookupKey] = jsonData
	err = session.Save(r, w)
	if err != nil {
		return fmt.Errorf("could not save session: %w", err)
	}

	return nil
}

// Get session data from the CookieStore with a lookup key.
func (s *CookieStore) Get(lookupKey string, r *http.Request) ([]byte, error) {
	session, err := s.store.Get(r, s.cookieName)
	if err != nil {
		return nil, fmt.Errorf("could not decode session: %w", err)
	}

	dataBytes, ok := session.Values[lookupKey].([]byte)
	if !ok {
		return nil, errors.New("could not cast session data to bytes")
	}

	delete(session.Values, lookupKey)
	return dataBytes, nil
}
