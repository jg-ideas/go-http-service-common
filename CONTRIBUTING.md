## CONTRIBUTING

1. Clone the repository and make your changes.
1. Commit messages are formatted using the conventional commits format, see [link](https://www.conventionalcommits.org/en/v1.0.0/).
1. Create a pull request.
