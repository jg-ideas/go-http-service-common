module gitlab.com/jg-ideas/go-http-service-common

go 1.15

require (
	github.com/gorilla/sessions v1.2.1
	go.uber.org/zap v1.16.0
)
