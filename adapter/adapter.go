package adapter

import (
	"context"
	"net/http"
	"runtime/debug"
	"time"

	"go.uber.org/zap"
)

// Adapter is a middleware type
type Adapter func(handler http.Handler) http.Handler

// Apply applies adapters (middlewares)
func Apply(handler http.Handler, adapters ...Adapter) http.Handler {
	for i := len(adapters) - 1; i >= 0; i-- {
		handler = adapters[i](handler)
	}
	return handler
}

// LogAdapter returns an Adapter that returns a logging middleware
func LogAdapter(logger *zap.Logger) Adapter {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				logger.Debug("http.Request",
					// todo add http status
					zap.String("method", r.Method),
					zap.String("path", r.URL.EscapedPath()),
					zap.String("user-agent", r.UserAgent()),
				)
				next.ServeHTTP(w, r)
			},
		)
	}
}

// TimeoutAdapter ensures that requests don't hang.
func TimeoutAdapter(timeout time.Duration) Adapter {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				ctx, cancelFn := context.WithTimeout(r.Context(), timeout)
				defer cancelFn()
				next.ServeHTTP(w, r.WithContext(ctx))
			},
		)
	}
}

// RecoveryAdapter returns an Adapter that handles panics with logging
func RecoveryAdapter(logger *zap.Logger) Adapter {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				defer func() {
					if err := recover(); err != nil {
						w.WriteHeader(http.StatusInternalServerError)
						logger.Fatal("recovered from error",
							zap.Any("error", err),
							zap.ByteString("trace", debug.Stack()),
						)
					}
				}()
				next.ServeHTTP(w, r)
			},
		)
	}
}
