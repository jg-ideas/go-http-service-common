## Go HTTP Service Common

Contains a common HTTP components for building web services.


## To make a release

```bash
$ VERSION=<next version> make release
$ git push
```